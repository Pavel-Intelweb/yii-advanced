<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>
        <?php if (Yii::$app->user->identity) {
            if (!$order && Yii::$app->user->id) { ?>

                <form action="<?= $data['payNowButtonUrl']; ?>" method="post">
                    <input type="hidden" name="cmd" value="_xclick">
                    <input type="hidden" name="business" value="<?= $data['receiverEmail']; ?>">
                    <input id="paypalItemName" type="hidden" name="item_name" value="<?= $data['itemName']; ?>">
                    <input id="paypalQuantity" type="hidden" name="quantity" value="<?= $data['quantity']; ?>">
                    <input id="paypalAmmount" type="hidden" name="amount" value="<?= $data['amount']; ?>">
                    <input type="hidden" name="no_shipping" value="1">
                    <input type="hidden" name="return" value="<?= $data['returnUrl']; ?>">

                    <input type="hidden" name="custom" value='<?= json_encode($data['customData']);?>'>

                    <input type="hidden" name="currency_code" value="USD">
                    <input type="hidden" name="lc" value="US">
                    <input type="hidden" name="bn" value="PP-BuyNowBF">

                    <button type="submit" class="btn btn-link">
                        Pay Now
                    </button>
                </form>
                <?php } elseif ($order && $order->user_id == Yii::$app->user->id && $user->downloaded == 0) { ?>
                    <?= Html::a('Download me', ['site/download'], ['class' => 'btn btn-primary', 'id' => 'downloadMe']) ?>
                <?php } else { ?>
                    <p>Thanks for downloading</p>
                 <?php } ?>
        <?php } else { ?>
            You must
            <?= Html::a('register', ['site/signup'], ['class' => 'btn btn-primary']) ?>
            or
            <?= Html::a('login', ['site/login'], ['class' => 'btn btn-success']) ?>
        <?php } ?>

    </div>

</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
    $('#downloadMe').click(function () {
        setTimeout(function () {
            location.reload();
        }, 1500);
    });
</script>
